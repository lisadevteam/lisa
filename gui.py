import sys

import time
from PyQt5.QtCore import QObject
from PyQt5.QtGui import QIcon
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtWidgets import QApplication


class Gui(object):
    def __init__(self):
        self.app = QApplication(['Lisa'])
        self.app.setWindowIcon(QIcon('res/gui/icon.png'))
        self.engine = QQmlApplicationEngine("res/gui/views/main/view.qml")
        self.rootObjects = self.engine.rootObjects()[0]
        self.engine.quit.connect(self.app.quit)

    def add_logs(self, logs):
        output_log = self.rootObjects.findChild(QObject, "outputLogs")
        current_logs = output_log.property("text")
        if output_log.property("contentHeight")+10 > output_log.property("height"):
            current_logs = ""
        if current_logs != "":
            current_logs += "\n"

        current_time = "["+time.strftime("%H:%M:%S")+"] "

        output_log.setProperty("text", current_logs + current_time + logs)

    def exec_(self):
        return self.app.exec_()

    def connect(self, thread, signal, callback):
        self.app.connect(thread, signal, callback)

    def exit(self):
        return self.app.exit()
