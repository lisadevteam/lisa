from urllib.parse import urljoin

import requests
from requests_aws4auth import AWS4Auth

IVONA_REGION_ENDPOINTS = {
    'eu-west-1': 'https://tts.eu-west-1.ivonacloud.com',
    'us-east-1': 'https://tts.us-east-1.ivonacloud.com',
    'us-west-2': 'https://tts.us-west-2.ivonacloud.com',
}


class IvonaAPIException(Exception):
    pass


class IvonaAPI(object):
    def __init__(self, access_key, secret_key, codec='mp3', region='eu-west-1'):
        self.access_key = access_key
        self.secret_key = secret_key
        self.region = region
        self.codec = codec.lower()

        self.aws4auth = AWS4Auth(
            self.access_key, self.secret_key,
            region, 'tts',
        )

        self.session = requests.Session()
        self.session.auth = self.aws4auth

        # 'x-slow', 'slow', 'medium', 'fast', 'x-fast', 'default'
        self.rate = 'default'

        # 'silent', 'x-soft', 'soft', 'medium', 'loud', 'x-loud', 'default'
        self.volume = 'default'

        # Integer in the range of 0-3000 (in milliseconds)
        self.sentence_break = 400
        # Integer in the range of 0-5000 (in milliseconds)
        self.paragraph_break = 650

    def _get_response(self, method, endpoint, data=None):
        url = urljoin(IVONA_REGION_ENDPOINTS[self.region], endpoint)
        response = getattr(self.session, method)(
            url, json=data,
        )

        if 'x-amzn-ErrorType' in response.headers:
            raise IvonaAPIException(response.headers['x-amzn-ErrorType'])

        if response.status_code != requests.codes.ok:
            raise IvonaAPIException(
                "Something wrong happened: {}".format(response.json())
            )

        return response

    def get_available_voices(self, language=None, gender=None):
        endpoint = "ListVoices"

        data = dict()
        if language:
            data.update({'Voice': {'Language': language}})

        if gender:
            data.update({'Voice': {'Gender': gender}})

        response = self._get_response('get', endpoint, data)

        return response.json()['Voices']

    def text_to_speech(self, text, language):
        endpoint = "CreateSpeech"

        if language == "en-US":
            voice_name = "Salli"
        elif language == "fr-FR":
            voice_name = "Celine"
        else:
            raise IvonaAPIException("language undefined")

        data = {
            'Input': {
                'Data': text,
            },
            'OutputFormat': {
                'Codec': self.codec.upper(),
            },
            'Parameters': {
                'Rate': self.rate,
                'Volume': self.volume,
                'SentenceBreak': self.sentence_break,
                'ParagraphBreak': self.paragraph_break,
            },
            'Voice': {
                'Name': voice_name,
                'Language': language,
            }
        }

        return self._get_response("post", endpoint, data).content
