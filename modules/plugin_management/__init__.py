import re

from stevedore import extension


class PluginManager(object):
    def __init__(self):
        self.plugins = []

    def load(self):
        self.plugins = extension.ExtensionManager(namespace="lisaplugins").extensions

    def plugins_count(self):
        return len(self.plugins)

    def list_plugins_description(self):
        response = ""

        for index, plugin in enumerate(self.plugins):
            response += plugin.plugin().get_description()
            if index is not len(self.plugins)-1:
                response += ", "

        return response

    def get_plugin_from_speech(self, speech, language):
        for plugin in self.plugins:
            for keyword in plugin.plugin().get_call_keywords()[language.value.code]:
                regex = re.compile(".*"+keyword+".*")
                if regex.match(speech) is not None:
                    return plugin.plugin()

        return None
