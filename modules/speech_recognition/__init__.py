import audioop
import collections
import io
import json
import math
import os
import platform
import shutil
import stat
import subprocess
import threading
import wave
from enum import Enum
from urllib.error import URLError, HTTPError
from urllib.parse import urlencode
from urllib.request import Request, urlopen

from lisa_sdk import UserSpeech
from pyaudio import paInt16, get_sample_size


class WaitTimeoutError(Exception):
    pass


class RequestError(Exception):
    pass


class UnknownValueError(Exception):
    pass


class UnknownRecognitionEngineError(Exception):
    pass


class AudioSource(object):
    def __init__(self):
        raise NotImplementedError("this is an abstract class")

    def __enter__(self):
        raise NotImplementedError("this is an abstract class")

    def __exit__(self, exc_type, exc_value, traceback):
        raise NotImplementedError("this is an abstract class")


class AudioData(object):
    def __init__(self, frame_data, sample_rate, sample_width):
        assert sample_rate > 0, "Must be a positive integer"
        assert sample_width % 1 == 0 and 1 <= sample_width <= 4, "Must be between 1 and 4 inclusive"
        self.frame_data = frame_data
        self.sample_rate = sample_rate
        self.sample_width = int(sample_width)

    def get_raw_data(self, convert_rate=None, convert_width=None):
        assert convert_rate is None or convert_rate > 0, "Must be a positive integer"
        assert convert_width is None or (
            convert_width % 1 == 0 and 1 <= convert_width <= 4), "Must be between 1 and 4 inclusive"

        raw_data = self.frame_data

        if self.sample_width == 1:
            raw_data = audioop.bias(raw_data, 1, -128)

        if convert_rate is not None and self.sample_rate != convert_rate:
            raw_data, _ = audioop.ratecv(raw_data, self.sample_width, 1, self.sample_rate, convert_rate, None)

        if convert_width is not None and self.sample_width != convert_width:
            if convert_width == 3:
                raw_data = audioop.lin2lin(raw_data, self.sample_width, 4)
                try:
                    audioop.bias(b"", 3,
                                 0)
                except audioop.error:
                    raw_data = b"".join(raw_data[i + 1:i + 4] for i in range(0, len(raw_data), 4))
                else:
                    raw_data = audioop.lin2lin(raw_data, self.sample_width, convert_width)
            else:
                raw_data = audioop.lin2lin(raw_data, self.sample_width, convert_width)

        if convert_width == 1:
            raw_data = audioop.bias(raw_data, 1, 128)

        return raw_data

    def get_flac_data(self, convert_rate=None, convert_width=None):
        assert convert_width is None or (
            convert_width % 1 == 0 and 1 <= convert_width <= 3), "Must be between 1 and 3 inclusive"

        if self.sample_width > 3 and convert_width is None:
            convert_width = 3

        wav_data = self.get_wav_data(convert_rate, convert_width)
        flac_converter = get_flac_converter()
        process = subprocess.Popen([
            flac_converter,
            "--stdout", "--totally-silent",
            "--best",
            "-"
        ], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        flac_data, stderr = process.communicate(wav_data)
        return flac_data

    def get_wav_data(self, convert_rate=None, convert_width=None):
        raw_data = self.get_raw_data(convert_rate, convert_width)
        sample_rate = self.sample_rate if convert_rate is None else convert_rate
        sample_width = self.sample_width if convert_width is None else convert_width

        with io.BytesIO() as wav_file:
            wav_writer = wave.open(wav_file, "wb")
            try:
                wav_writer.setframerate(sample_rate)
                wav_writer.setsampwidth(sample_width)
                wav_writer.setnchannels(1)
                wav_writer.writeframes(raw_data)
                wav_data = wav_file.getvalue()
            finally:
                wav_writer.close()
        return wav_data


class Microphone(AudioSource):
    def __init__(self, pyaudio, device_index=None, sample_rate=16000, chunk_size=1024):
        self.pyaudio = pyaudio

        assert device_index is None or isinstance(device_index, int), "Must be None or an integer"
        if device_index is not None:
            try:
                count = self.pyaudio.get_device_count()
            except:
                self.pyaudio.terminate()
                raise
            assert 0 <= device_index < count, "Should be between 0 and {} inclusive)".format(
                count, count - 1)
        assert isinstance(sample_rate, int) and sample_rate > 0, "Must be a positive integer"
        assert isinstance(chunk_size, int) and chunk_size > 0, "Must be a positive integer"
        self.device_index = device_index
        self.format = paInt16
        self.SAMPLE_WIDTH = get_sample_size(self.format)
        self.SAMPLE_RATE = sample_rate
        self.CHUNK = chunk_size

        self.audio = None
        self.stream = None

    def __enter__(self):
        assert self.stream is None, "This audio source is already inside a context manager"
        self.audio = self.pyaudio
        try:
            self.stream = Microphone.MicrophoneStream(
                self.audio.open(
                    input_device_index=self.device_index, channels=1,
                    format=self.format, rate=self.SAMPLE_RATE, frames_per_buffer=self.CHUNK,
                    input=True
                )
            )
        except:
            self.audio.terminate()
            raise
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self.stream.close()
        finally:
            self.stream = None

    class MicrophoneStream(object):
        def __init__(self, pyaudio_stream):
            self.pyaudio_stream = pyaudio_stream

        def read(self, size):
            return self.pyaudio_stream.read(size, exception_on_overflow=False)

        def close(self):
            try:
                if not self.pyaudio_stream.is_stopped():
                    self.pyaudio_stream.stop_stream()
            finally:
                self.pyaudio_stream.close()


class Recognizer(AudioSource):
    def __init__(self):
        self.energy_threshold = 300
        self.dynamic_energy_threshold = True
        self.dynamic_energy_adjustment_damping = 0.15
        self.dynamic_energy_ratio = 1.5
        self.pause_threshold = 0.8
        self.operation_timeout = None

        self.phrase_threshold = 0.3
        self.non_speaking_duration = 0.5

    def record(self, source, duration=None, offset=None):
        assert isinstance(source, AudioSource), "Must be an audio source"
        assert source.stream is not None, "Must be entered before recording"

        frames = io.BytesIO()
        seconds_per_buffer = (source.CHUNK + 0.0) / source.SAMPLE_RATE
        elapsed_time = 0
        offset_time = 0
        offset_reached = False
        while True:
            if offset and not offset_reached:
                offset_time += seconds_per_buffer
                if offset_time > offset:
                    offset_reached = True

            buffer = source.stream.read(source.CHUNK)
            if len(buffer) == 0:
                break

            if offset_reached or not offset:
                elapsed_time += seconds_per_buffer
                if duration and elapsed_time > duration:
                    break

                frames.write(buffer)

        frame_data = frames.getvalue()
        frames.close()
        return AudioData(frame_data, source.SAMPLE_RATE, source.SAMPLE_WIDTH)

    def adjust_for_ambient_noise(self, source, duration=1):
        assert isinstance(source, AudioSource), "Must be an audio source"
        assert source.stream is not None, "Must be entered before adjusting"
        assert self.pause_threshold >= self.non_speaking_duration >= 0

        seconds_per_buffer = (source.CHUNK + 0.0) / source.SAMPLE_RATE
        elapsed_time = 0

        while True:
            elapsed_time += seconds_per_buffer
            if elapsed_time > duration:
                break
            buffer = source.stream.read(source.CHUNK)
            energy = audioop.rms(buffer, source.SAMPLE_WIDTH)

            damping = self.dynamic_energy_adjustment_damping ** seconds_per_buffer
            target_energy = energy * self.dynamic_energy_ratio
            self.energy_threshold = self.energy_threshold * damping + target_energy * (1 - damping)

    def listen(self, source, timeout=None, phrase_time_limit=None):
        assert isinstance(source, AudioSource), "Must be an audio source"
        assert source.stream is not None, "Must be entered before listening"
        assert self.pause_threshold >= self.non_speaking_duration >= 0

        seconds_per_buffer = (source.CHUNK + 0.0) / source.SAMPLE_RATE
        pause_buffer_count = int(math.ceil(
            self.pause_threshold / seconds_per_buffer))
        phrase_buffer_count = int(math.ceil(
            self.phrase_threshold / seconds_per_buffer))
        non_speaking_buffer_count = int(math.ceil(
            self.non_speaking_duration / seconds_per_buffer))

        elapsed_time = 0
        buffer = b""
        while True:
            frames = collections.deque()

            while True:
                elapsed_time += seconds_per_buffer
                if timeout and elapsed_time > timeout:
                    raise WaitTimeoutError("listening timed out while waiting for phrase to start")

                buffer = source.stream.read(source.CHUNK)
                if len(buffer) == 0:
                    break
                frames.append(buffer)
                if len(frames) > non_speaking_buffer_count:
                    frames.popleft()

                energy = audioop.rms(buffer, source.SAMPLE_WIDTH)
                if energy > self.energy_threshold:
                    break

                if self.dynamic_energy_threshold:
                    damping = self.dynamic_energy_adjustment_damping ** seconds_per_buffer
                    target_energy = energy * self.dynamic_energy_ratio
                    self.energy_threshold = self.energy_threshold * damping + target_energy * (1 - damping)

            pause_count, phrase_count = 0, 0
            phrase_start_time = elapsed_time
            while True:
                elapsed_time += seconds_per_buffer
                if phrase_time_limit and elapsed_time - phrase_start_time > phrase_time_limit:
                    break

                buffer = source.stream.read(source.CHUNK)
                if len(buffer) == 0:
                    break
                frames.append(buffer)
                phrase_count += 1

                energy = audioop.rms(buffer, source.SAMPLE_WIDTH)
                if energy > self.energy_threshold:
                    pause_count = 0
                else:
                    pause_count += 1
                if pause_count > pause_buffer_count:
                    break

            phrase_count -= pause_count
            if phrase_count >= phrase_buffer_count or len(buffer) == 0:
                break

        for i in range(pause_count - non_speaking_buffer_count):
            frames.pop()
        frame_data = b"".join(list(frames))

        return AudioData(frame_data, source.SAMPLE_RATE, source.SAMPLE_WIDTH)

    def listen_in_background(self, source, callback):
        assert isinstance(source, AudioSource), "Must be an audio source"
        running = [True]

        def threaded_listen():
            with source as s:
                while running[0]:
                    try:
                        audio = self.listen(s, 1)
                    except WaitTimeoutError:
                        pass
                    else:
                        if running[0]:
                            callback(self, audio)

        def stopper():
            running[0] = False
            listener_thread.join()

        listener_thread = threading.Thread(target=threaded_listen)
        listener_thread.daemon = True
        listener_thread.start()
        return stopper

    def recognize_sphinx(self, audio_data):
        assert isinstance(audio_data, AudioData), "Must be audio data"

        try:
            from pocketsphinx import pocketsphinx
        except ImportError:
            raise RequestError("missing PocketSphinx module")
        except ValueError:
            raise RequestError(
                "bad PocketSphinx installation detected")

        language_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pocketsphinx")
        if not os.path.isdir(language_directory):
            raise RequestError("missing PocketSphinx language data directory: \"{}\"".format(language_directory))
        acoustic_parameters_directory = os.path.join(language_directory, "acoustic-model")
        if not os.path.isdir(acoustic_parameters_directory):
            raise RequestError("missing PocketSphinx language model parameters directory: \"{}\"".format(
                acoustic_parameters_directory))
        language_model_file = os.path.join(language_directory, "language-model.lm.bin")
        if not os.path.isfile(language_model_file):
            raise RequestError("missing PocketSphinx language model file: \"{}\"".format(language_model_file))
        phoneme_dictionary_file = os.path.join(language_directory, "pronounciation-dictionary.dict")
        if not os.path.isfile(phoneme_dictionary_file):
            raise RequestError("missing PocketSphinx phoneme dictionary file: \"{}\"".format(phoneme_dictionary_file))

        config = pocketsphinx.Decoder.default_config()
        config.set_string("-hmm", acoustic_parameters_directory)
        config.set_string("-lm", language_model_file)
        config.set_string("-dict", phoneme_dictionary_file)
        config.set_string("-logfn", os.devnull)
        decoder = pocketsphinx.Decoder(config)

        raw_data = audio_data.get_raw_data(convert_rate=16000, convert_width=2)

        decoder.start_utt()
        decoder.process_raw(raw_data, False, True)
        decoder.end_utt()

        hypotheses = {}

        hypothesis = decoder.hyp()
        if hypothesis is not None:
            hypotheses[1] = hypothesis.hypstr
            return hypotheses
        raise UnknownValueError()

    def recognize_google(self, audio_data, key, language):
        assert isinstance(audio_data, AudioData), "Must be audio data"
        assert isinstance(key, str), "Must be ``None`` or a string"
        assert isinstance(language, str), "Must be a string"

        flac_data = audio_data.get_flac_data(
            convert_rate=None if audio_data.sample_rate >= 8000 else 8000,
            convert_width=2
        )

        url = "http://www.google.com/speech-api/v2/recognize?{}".format(urlencode({
            "client": "chromium",
            "lang": language,
            "key": key,
        }))
        request = Request(url, data=flac_data,
                          headers={"Content-Type": "audio/x-flac; rate={}".format(audio_data.sample_rate)})

        try:
            response = urlopen(request, timeout=self.operation_timeout)
        except HTTPError as e:
            raise RequestError("recognition request failed: {}".format(e.reason))
        except URLError as e:
            raise RequestError("recognition connection failed: {}".format(e.reason))
        response_text = response.read().decode("utf-8")

        actual_result = []
        for line in response_text.split("\n"):
            if not line:
                continue
            result = json.loads(line)["result"]
            if len(result) != 0:
                actual_result = result[0]
                break

        if len(actual_result) == 0 or len(actual_result.get("alternative", [])) == 0:
            raise UnknownValueError()

        hypotheses = {}

        for hypothesis in actual_result.get("alternative", []):
            hypotheses[hypothesis.get("confidence")] = hypothesis.get("transcript")

        return hypotheses


def get_flac_converter():
    flac_converter = shutil.which("flac")
    if flac_converter is None:
        base_path = os.path.dirname(os.path.abspath(
            __file__))
        system, machine = platform.system(), platform.machine()
        if system == "Windows" and machine in {"i686", "i786", "x86", "x86_64", "AMD64"}:
            flac_converter = os.path.join(base_path, "flac", "flac-win32.exe")
        elif system == "Darwin" and machine in {"i686", "i786", "x86", "x86_64", "AMD64"}:
            flac_converter = os.path.join(base_path, "flac", "flac-mac")
        elif system == "Linux" and machine in {"i686", "i786", "x86"}:
            flac_converter = os.path.join(base_path, "flac", "flac-linux-x86")
        elif system == "Linux" and machine in {"x86_64", "AMD64"}:
            flac_converter = os.path.join(base_path, "flac", "flac-linux-x86_64")
        else:
            raise OSError("FLAC conversion utility not available")

    try:
        stat_info = os.stat(flac_converter)
        os.chmod(flac_converter, stat_info.st_mode | stat.S_IEXEC)
    except OSError:
        pass

    return flac_converter


class RecognitionEngine(Enum):
    SPHINX = "pocketsphinx"
    GOOGLE = "google_api"


class SpeechRecognition(object):
    def __init__(self, pyaudio, google_api_key):
        self.pyaudio = pyaudio
        self.recognizer = Recognizer()
        self.google_api_key = google_api_key

    def listen(self, recognition_engine, language):
        with Microphone(self.pyaudio) as source:
            time_limit = None
            if recognition_engine == RecognitionEngine.SPHINX:
                time_limit = 3
            audio = self.recognizer.listen(source, None, time_limit)

        try:
            if recognition_engine == RecognitionEngine.SPHINX:
                speech = self.recognizer.recognize_sphinx(audio)
            elif recognition_engine == RecognitionEngine.GOOGLE:
                speech = self.recognizer.recognize_google(audio, self.google_api_key, language.lower())
            else:
                print("Error: engine '{0}' is undefined".format(recognition_engine))
                raise UnknownRecognitionEngineError

        except UnknownValueError:
            raise
        except RequestError as error:
            print("{0} error: {1}".format(recognition_engine, error))
            raise

        return UserSpeech(speech, language)
