import errno
import os
import random
import string
import wave

from pydub import AudioSegment


def create_directory(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def safe_open_w(path, binary=False):
    create_directory(os.path.dirname(path))

    write_mode = 'w'

    if binary:
        write_mode = 'wb'

    return open(path, write_mode)


def random_string(length):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(length))


def convert_mp3_to_wav(mp3_path):
    mp3_file = AudioSegment.from_mp3(mp3_path)

    wav_path = mp3_path.replace(".mp3", ".wav")

    mp3_file.export(wav_path, format="wav")

    return wav_path


def play_wav(pyaudio, path):
    CHUNK = 1024

    file = wave.open(path, 'rb')

    stream = pyaudio.open(format=pyaudio.get_format_from_width(file.getsampwidth()),
                          channels=file.getnchannels(),
                          rate=file.getframerate(),
                          output=True)

    data = file.readframes(CHUNK)

    while len(data) > 0:
        stream.write(data)
        data = file.readframes(CHUNK)

    stream.stop_stream()
    stream.close()


def clear_temp_directory():
    for the_file in os.listdir("./.temp"):
        file_path = os.path.join("./.temp", the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)
            raise
