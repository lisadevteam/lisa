import QtQuick 2.3
import QtQuick.Window 2.2

Window {
    id: rootWindow
    width: 700
    height: 416
    color: "#aed581"
    title: "Lisa"
    visible: true
    flags: Qt.FramelessWindowHint

    MouseArea {
        property variant clickPos: "1,1"
        width: 700
        height: titleBar.height
        onPressed: {
            clickPos = Qt.point(mouse.x, mouse.y);
        }
        onPositionChanged: {
            var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y);
            rootWindow.x += delta.x;
            rootWindow.y += delta.y;
        }

        Rectangle {
            id: titleBar
            x: 0
            y: 0
            width: 700
            height: 17
            color: "#8bc34a"

            MouseArea {
                id: minimizeArea
                x: 645
                y: 0
                width: minimizeButton.width
                height: minimizeButton.height
                onClicked: {
                    rootWindow.showMinimized()
                }

                Image {
                    id: minimizeButton
                    x: 0
                    y: 0
                    width: 17
                    height: 17
                    source: "images/minimize.png"
                }
            }

            MouseArea {
                id: closeArea
                x: 670
                y: 0
                width: closeButton.width
                height: closeButton.height
                onClicked: {
                    Qt.quit();
                }

                Image {
                    id: closeButton
                    x: 0
                    y: 0
                    width: 17
                    height: 17
                    z: 2
                    visible: true
                    source: "images/close.png"
                }
            }

            Image {
                id: leafLogo
                x: 5
                y: 1
                width: 15
                height: 15
                opacity: 0.6
                antialiasing: true
                source: "images/leaf-blk.png"
            }

            Text {
                id: title
                x: 203
                y: 2
                text: qsTr("Lisa - Lightweight Improvable Software Assistant")
                font.family: "Verdana"
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 12
            }


        }
    }

    Rectangle {
        id: borderLeft
        x: 0
        y: 17
        width: 3
        height: 400
        color: "#8bc34a"
    }

    Rectangle {
        id: borderRight
        x: 697
        y: 17
        width: 3
        height: 400
        color: "#8bc34a"
    }

    Rectangle {
        id: borderBottom
        x: 3
        y: 414
        width: 694
        height: 3
        color: "#8bc34a"
    }

    Image {
        id: backgroundLogo
        x: 79
        y: 76
        width: 557
        height: 280
        opacity: 0.3
        source: "images/lisa-wht.png"
    }

    Rectangle {
        id: outputLogsTitleBackground
        x: 3
        y: 232
        width: 694
        height: 19
        color: "#000000"
        opacity: 0.8

        Text {
            id: outputLogsTitle
            x: 3
            y: 3
            color: "#cdcdcd"
            text: qsTr("Output Logs")
            font.italic: false
            font.bold: true
            font.pixelSize: 12
        }
    }

    Rectangle {
        id: outputLogsBackground
        x: 3
        y: 251
        width: 694
        height: 162
        color: "#000000"
        opacity: 0.7

        TextEdit {
            id: outputLogs
            objectName: "outputLogs"
            x: 6
            y: 5
            width: 682
            height: 152
            color: "#c6c6c6"
            text: ""
            activeFocusOnPress: false
            cursorVisible: false
            readOnly: true
            font.family: "Arial"
            textFormat: Text.PlainText
            font.pixelSize: 14
            wrapMode: TextEdit.Wrap
        }
    }
}
