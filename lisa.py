import os
import re
import sys
from PyQt5 import QtCore
from enum import Enum

from PyQt5.QtCore import QThread
from lisa_sdk import SpeechResponse, UserSpeech, LisaLanguages
from pyaudio import PyAudio

from gui import Gui
from modules.plugin_management import PluginManager
from modules.speech_recognition import RecognitionEngine, UnknownValueError, SpeechRecognition
from modules.speech_synthesis import IvonaAPI
from modules.utils import convert_mp3_to_wav
from modules.utils import random_string, play_wav, clear_temp_directory
from modules.utils import safe_open_w


class LisaMode(Enum):
    LISTENING_BUILTIN = "listening_builtin_mode"
    LISTENING_PLUGIN = "listening_plugin_mode"
    SLEEPING = "sleeping_mode"
    SHUTTING_DOWN = "shutting_down_mode"


class Lisa(object):
    built_in_patterns = {
        "quitter": 0,
        "au revoir": 0,
        "^lisa$": 1,
        ".*veille": 2,
        ".*list[e]?[s]?.*module[s]?": 3,
        ".*combien.*module[s]?.*(?:charg(?:[e]|[é])[s]?|disponible[s]?)": 4,
        ".*(?:recharge|charge)[s]?.*module[s]?": 5,
        ".*pr(?:é|e)sente toi": 6,
        ".*pr(?:é|e)sentation": 6,
        ".*(?:qu'est ce que|qui) tu es": 6,
        "^module[s]?": 7
    }

    def __init__(self, language, pyaudio, speech_recognition, speech_synthesis, print_log):
        self.language = language
        self.plugin_manager = PluginManager()
        self.plugin_manager.load()
        self.currentPlugin = None
        self.speech_recognition = speech_recognition
        self.speech_synthesis = speech_synthesis
        self.print_log = print_log
        self.currentMode = LisaMode.SLEEPING
        self.pyaudio = pyaudio

        if speech_recognition is None:
            self.simulate_confidence = input("Recognition simulation enabled, simulate confidence ? (yes/no): ")
        clear_temp_directory()

        print("Waiting to hear \"lisa\" keyword..")

    def simulate_recognition(self):
        print("-- Simulating Speech Recognition --")
        hypotheses = {}

        if self.simulate_confidence == "yes":
            count = int(input("How many speech hypothesis do you want to enter? "))

            for i in range(count):
                confidence = float(input("Enter confidence level (0-1): "))
                hypotheses[confidence] = input("Enter hypothesis: ")
        else:
            hypotheses[1] = input("Enter something: ")

        return UserSpeech(hypotheses, self.language)

    def listen(self):
        while True:
            try:
                if self.speech_recognition is not None:
                    if self.currentMode == LisaMode.SLEEPING:
                        user_speech = self.speech_recognition.listen(RecognitionEngine.SPHINX, self.language.value.code)
                    else:
                        user_speech = self.speech_recognition.listen(RecognitionEngine.GOOGLE, self.language.value.code)
                else:
                    user_speech = self.simulate_recognition()
                break
            except UnknownValueError:
                if self.currentMode != LisaMode.SLEEPING:
                    print("Please, try again")

        for key, speech in user_speech.speeches.items():
            user_speech.speeches[key] = speech.lower()

        return user_speech

    def process_built_in(self, user_speech):
        hypotheses_matched = {}
        best_hypotheses = ""
        response = "Je n'ai pas compris"

        if self.currentMode == LisaMode.SLEEPING:
            response = ""

        for confidence, speech in user_speech.speeches.items():
            for pattern in self.built_in_patterns:
                if re.match(pattern, speech) is not None:
                    hypotheses_matched[confidence] = speech
                    break

        if len(hypotheses_matched) >= 1:
            best_hypotheses = hypotheses_matched[max(hypotheses_matched.keys(), key=float)]

        if best_hypotheses != "":
            order_id = -1
            for regex_str in self.built_in_patterns.keys():
                regex = re.compile(regex_str)
                if regex.match(best_hypotheses) is not None:
                    order_id = self.built_in_patterns[regex_str]

            if self.currentMode == LisaMode.LISTENING_BUILTIN:
                if order_id == 0:
                    self.currentMode = LisaMode.SHUTTING_DOWN
                    return SpeechResponse("Au revoir")
                if order_id == 2:
                    self.currentMode = LisaMode.SLEEPING
                    return SpeechResponse("Passage en mode veille")
                if order_id == 3:
                    plugins_count = self.plugin_manager.plugins_count()

                    if plugins_count > 0:
                        return SpeechResponse(
                            "Voici les modules chargés: " + self.plugin_manager.list_plugins_description())

                    return SpeechResponse("Il n'y a aucun module disponible")
                if order_id == 4:
                    plugins_count = self.plugin_manager.plugins_count()
                    return SpeechResponse("Il y a " + str(plugins_count) + ", modules disponibles")
                if order_id == 5:
                    self.plugin_manager.load()
                    plugins_count = self.plugin_manager.plugins_count()

                    if plugins_count > 1:
                        return SpeechResponse(str(plugins_count) + " modules ont été chargés")
                    elif plugins_count == 1:
                        return SpeechResponse("1 module a été chargé")
                    return SpeechResponse("Aucun module disponible")
                if order_id == 6:
                    return SpeechResponse(
                        "Je suis Lisa, un assistant virtuel développé en Python qui tiens à être léger et " +
                        "personnalisable via un système de modules. J'ai été créée en 2016 dans un cadre " +
                        "scolaire. Pour plus d'information veuillez vous rendre dans la section à propos " +
                        "du panneau de configuration.")
                if order_id == 7:
                    response = "Le module demandé n'a pas été reconnu"
                    plugin = self.plugin_manager.get_plugin_from_speech(best_hypotheses, self.language)

                    if plugin is not None:
                        self.currentPlugin = plugin
                        self.currentMode = LisaMode.LISTENING_PLUGIN
                        response = plugin.get_description() + " activé"

                    return SpeechResponse(response)
            elif order_id == 1:
                self.currentMode = LisaMode.LISTENING_BUILTIN
                return SpeechResponse("Oui ?")

        return SpeechResponse(response)

    def process(self, user_speech):
        if self.currentMode == LisaMode.LISTENING_BUILTIN or self.currentMode == LisaMode.SLEEPING:
            return self.process_built_in(user_speech)

        return SpeechResponse("")

    def speak(self, response):
        if response.speech == "":
            return

        if response.language is None:
            response.language = self.language

        audio = self.speech_synthesis.text_to_speech(response.speech, response.language.value.code)

        filename = random_string(10)

        with safe_open_w(".temp/" + filename + ".mp3", True) as file:
            file.write(audio)
            file.close()

        self.print_log("Lisa: " + response.speech.capitalize())
        play_wav(self.pyaudio, convert_mp3_to_wav(".temp/" + filename + ".mp3"))

        clear_temp_directory()


class LisaLoop(QThread):
    outputLogSignal = QtCore.pyqtSignal(str)

    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()

    def print_log(self, log):
        self.outputLogSignal.emit(log)

    def run(self):
        pyaudio = PyAudio()
        speech_recognition = SpeechRecognition(pyaudio, "AIzaSyBZm7HjoFF2VawX9Oh-BbAb0JCrsJtW2f0")
        speech_synthesis = IvonaAPI("GDNAJXTPF7S3XMO3NAPQ", "5ZhDPbuIjjkoycBnwGVgXJWHdVNKcDIkfKY5WcYD")

        if len(sys.argv) > 1 and sys.argv[1] == "simulate_recognition":
            speech_recognition = None
        lisa = Lisa(LisaLanguages.FRENCH, pyaudio, speech_recognition, speech_synthesis, self.print_log)

        while True:
            speech = lisa.listen()
            response = lisa.process(speech)
            lisa.speak(response)

            if lisa.currentMode == LisaMode.SHUTTING_DOWN:
                break


def main():
    gui = Gui()
    lisa = LisaLoop()
    lisa.finished.connect(gui.exit)
    lisa.outputLogSignal.connect(gui.add_logs)
    lisa.start()
    sys.exit(gui.exec_())


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            # noinspection PyProtectedMember
            os._exit(0)
